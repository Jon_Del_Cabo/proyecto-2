from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import os
from shutil import rmtree


window = Tk()
window.title("Aplicación Zymvol")
window.configure(bg="#D1D6D6")
window.geometry("1920x1080")

lbl = Label(window,
            text="Nombre de la aplicación",
            font=("Arial", 25), bg="#2193D5",
            fg="black",
            anchor="w")

lbl.place(x=270,
          y=100,
          width=1400)

List = Listbox(window,
               font=("Arial", 20))

List.place(x=320,
           y=240,
           width=1300,
           height=600)


def crear_proyecto():
    NewWindow = Toplevel()
    NewWindow.geometry("500x400")
    NewWindow.configure(bg="#D1D6D6")
    lbl10 = Label(NewWindow,
                  text="¿Tipo de proyecto?",
                  font=("Arial", 20),
                  bg="#D1D6D6",
                  fg="black")

    lbl10.place(x=20,
                y=20)

    opciones = ttk.Combobox(NewWindow,
                        state="readonly")
    opciones["values"] = ["Proyecto A", "Proyecto B"]

    opciones.set("Seleccionar")
    opciones.place(x=20,
                   y=80)
    lbl33 = Label(NewWindow,
                  text="Según el tipo de proyecto que selecciones, A o B, aqui se mostrará su "
                       "correspondiente definición y contenido.",
                  bg="white",
                  fg="black",
                  font=("Arial", 10),
                  wraplength=137,
                  justify="left")
    lbl33.place(x=310,
                y=80,
                height=150)

    def modified(event):
        if (event.widget.get() == "Seleccionar"):
            lbl33.config(text="Según el tipo de proyecto que selecciones, A o B, aqui se mostrará su "
                              "correspondiente definición y contenido.")

            btn32.config(messagebox.showwarning("", "Seleccione uno de los dos proyectos para poder continuar"))

        elif (event.widget.get() == "Proyecto A"):
            lbl33.config(text="Al seleccionar el Proyecto A va a obtener una serie de carpetas "
                              "específicas, relacionadas con el objetivo que usted, el usuario, "
                              "quiere obtener de su trabajo")
            def A():
                os.makedirs("D:/DirectorioPrincipalProyectoA")
                os.mkdir("D:/DirectorioPrincipalProyectoA/JavaScript")
                os.mkdir("D:/DirectorioPrincipalProyectoA/HTML")
                os.mkdir("D:/DirectorioPrincipalProyectoA/CSS")

                proyectoA = Toplevel()
                proyectoA.geometry("600x400")
                proyectoA.configure(bg="#D1D6D6")
                proyectoA.title("Proyecto A")
                NewWindow.destroy()

                lbl78 = Label(proyectoA,
                              text="¡El proyecto se ha descargado correctamente!",
                              bg="#D1D6D6",
                              fg="black",
                              font=("Arial", 15))
                lbl78.place(x=100,
                            y=100)
                lbl64 = Label(proyectoA,
                              text="El directorio raíz del proyecto se encuentra en D:/",
                              bg="#D1D6D6",
                              fg="black",
                              font=("Arial", 10))
                lbl64.place(x=150,
                            y=150)

                List.insert(0, "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "    
                               "       Status: Cerrado")
                List.itemconfigure(0, bg="silver",
                                   fg="black")

            btn32.config(command=A)

        elif (event.widget.get() == "Proyecto B"):
            lbl33.config(text="Al seleccionar el Proyecto B va a obtener una serie de carpetas "
                              "específicas, relacionadas con el objetivo que usted, el usuario, "
                              "quiere obtener de su trabajo")

            def B():
                os.makedirs("D:/DirectorioPrincipalProyectoB")
                os.mkdir("D:/DirectorioPrincipalProyectoB/main")
                os.mkdir("D:/DirectorioPrincipalProyectoB/.idea")
                os.mkdir("D:/DirectorioPrincipalProyectoB/venv")

                proyectoB = Toplevel()
                proyectoB.geometry("600x400")
                proyectoB.configure(bg="#D1D6D6")
                proyectoB.title("Proyecto B")
                NewWindow.destroy()

                lbl79 = Label(proyectoB,
                              text="¡El proyecto se ha descargado correctamente!",
                              bg="#D1D6D6",
                              fg="black",
                              font=("Arial", 15))
                lbl79.place(x=100,
                            y=100)

                lbl65 = Label(proyectoB,
                              text="El directorio raíz del proyecto se encuentra en D:/",
                              bg="#D1D6D6",
                              fg="black",
                              font=("Arial", 10))
                lbl65.place(x=150,
                            y=150)

                List.insert(0, "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                               "       Status: Cerrado")
                List.itemconfigure(0, bg="silver",
                                   fg="black")

            btn32.config(command=B)

    opciones.bind('<<ComboboxSelected>>', modified)

    btn32 = Button(NewWindow,
                   text="Finalizar",
                   bg="black",
                   fg="white",
                   font=("Arial", 10))
    btn32.place(x=410,
                y=350)


btn = Button (window,
              text="Crear proyecto",
              font=("Arial", 12),
              bg="black",
              fg="white",
              command=crear_proyecto)

btn.place(x=270,
          y=165)

if (os.path.exists("D:/DirectorioPrincipalProyectoA")):
    List.insert(0, "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Cerrado")
    List.itemconfigure(0, bg="silver",
                       fg="black")

if (os.path.exists("D:/DirectorioPrincipalProyectoB")):
    List.insert(0, "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Cerrado")
    List.itemconfigure(0, bg="silver",
                       fg="black")


def abrir ():
    if (List.get(ANCHOR) == "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Cerrado"):
        os.startfile("D:/DirectorioPrincipalProyectoA")
        List.delete(ANCHOR)
        List.insert(0, "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Abierto")
        List.itemconfig(0, bg="silver",
                        fg="black")

    elif (List.get(ANCHOR) == "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Cerrado"):
        os.startfile("D:/DirectorioPrincipalProyectoB")
        List.delete(ANCHOR)
        List.insert(0, "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Abierto")
        List.itemconfig(0, bg="silver",
                        fg="black")
def cerrar ():
    if (List.get(ANCHOR) == "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Abierto"):
        List.delete(ANCHOR)
        List.insert(0, "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Cerrado")
        List.itemconfig(0, bg="silver",
                        fg="black")
        messagebox.showinfo("Información", "El proyecto se ha cerrado y cifrado correctamente.")
    elif (List.get(ANCHOR) == "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Abierto"):
        List.delete(ANCHOR)
        List.insert(0, "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Cerrado")
        List.itemconfig(0, bg="silver",
                        fg="black")
        messagebox.showinfo("Información", "El proyecto se ha cerrado y cifrado correctamente.")

btn3 = Button(window,
              text="Cerrar proyecto",
              font=("Arial", 12),
              bg="black",
              fg="white",
              command=cerrar)

btn3.place(x=1545, y=165)

btn2 = Button(window,
              text="Abrir proyecto",
              font=("Arial", 12),
              bg="black",
              fg="white",
              command=abrir)

btn2.place(x=1415, y=165)

def eliminar ():
    if (List.get(ANCHOR) == "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Cerrado"):
        respuesta = messagebox.askyesno("IMPORTANTE", "¿Está seguro que quiere eliminar el proyecto?")
        if (respuesta == True):
            rmtree("D:/DirectorioPrincipalProyectoA")
            List.delete(ANCHOR)

    elif (List.get(ANCHOR) == "          ID: Proyecto A            Ubicación: D:/DirectorioPrincipalProyectoA        "
                   "       Status: Abierto"):
        respuesta = messagebox.askyesno("IMPORTANTE", "¿Está seguro que quiere eliminar el proyecto?")
        if (respuesta == True):
            rmtree("D:/DirectorioPrincipalProyectoA")
            List.delete(ANCHOR)

    if (List.get(ANCHOR) == "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Cerrado"):
        respuesta = messagebox.askyesno("IMPORTANTE", "¿Está seguro que quiere eliminar el proyecto?")
        if (respuesta == True):
            rmtree("D:/DirectorioPrincipalProyectoB")
            List.delete(ANCHOR)

    elif (List.get(ANCHOR) == "          ID: Proyecto B            Ubicación: D:/DirectorioPrincipalProyectoB        "
                   "       Status: Abierto"):
        respuesta = messagebox.askyesno("IMPORTANTE", "¿Está seguro que quiere eliminar el proyecto?")
        if (respuesta == True):
            rmtree("D:/DirectorioPrincipalProyectoB")
            List.delete(ANCHOR)

btn80 = Button(window,
               bg="black",
               fg="White",
               text="Eliminar proyecto",
               font=("Arial", 12),
               command=eliminar)
btn80.place(x=410,
            y=165)

window.mainloop()
